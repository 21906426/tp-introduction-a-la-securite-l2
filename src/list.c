#include <stdlib.h>

struct cell_t {
  void* val;
  unsigned long int id;
  struct cell_t* next;
};

typedef struct cell_t* list_t;

list_t list_empty(){
	return NULL;
}

//1 vrai, 0 faux
int list_is_empty(list_t l){
	return l == NULL;
}

list_t list_push(list_t l, void* x){
	list_t head = malloc(sizeof(struct cell_t));
	head->val = x;
	if(list_is_empty(l)){
		head->id = 1;
		head->next = NULL;
	}else{
		head->id = l->id + 1;
		head->next = l;
	}
	return head;
}

list_t list_tail(list_t l){
	if(list_is_empty(l)){
		return NULL;
	}else{
		return l->next;
	}
}

//revoie la valeur du premier élément
void* list_top(list_t l){
	if(list_is_empty(l)){
		return NULL;
	}else{
		return l->val;
	}
}

// return the found element or NULL
//renvoyer la valeur 
void* list_in(list_t l, void* x, int (*eq)(void*, void*)){
	while(!list_is_empty(l)){
		if(eq(l->val,x)){
			return l->val;
		}else{
			l = l-> next;
		}
	}
	return NULL;
}

unsigned long int list_len(list_t l){
	if(list_is_empty(l)){
		return 0;
	}else{
		return l->id;
	}
}


void* list_pop(list_t* l){ 
	if(list_is_empty(*l))
		return NULL;
	void* res = (*l)->val;
	list_t new_head = (*l)->next;
	free(*l);
	*l = new_head;
	return res;
}


void list_destroy(list_t l, void (*free)(void*)){
	while(!list_is_empty(l))
		free(list_pop(&l));
}

