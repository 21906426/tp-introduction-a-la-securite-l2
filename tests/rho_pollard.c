#include <stdio.h>
#include "bignum.h"

//Calcule le PGCD
unsigned long int gcd(unsigned long int a, unsigned long int b){
  unsigned long int old_r, r = 1;
  if(a < b)
    return gcd(b,a);

  while(r != 0){
    old_r = r;
    r = a % b;
    a = b;
    b = r;  
  }

  return old_r;
}

unsigned long int f(unsigned long int x){
	return x*x+1;
}

bignum_t f_bignum(bignum_t x){
	bignum_t d;
	d = bignum_from_int(1);
	return bignum_add(bignum_mul(x,x),d);
}

unsigned long int floyd(unsigned long int n){
	unsigned long int x = 145l,y =145l;
	unsigned long int d=1l;
	while(d==1l){
		x = f(x) % n;
		y = f(f(y)) % n;
		//printf("%d ",x);
		//printf(" %d ",y);
		//printf(" %d\n", d);
		d = gcd(x-y,n);
	}
	return d;
}

bignum_t floyd_bignum(bignum_t n){
	bignum_t x, y, d, one;
	x = bignum_from_int(30);
	y = bignum_from_int(40);
	d = bignum_from_int(1);
	one = bignum_from_int(1);
	while(bignum_eq(d,one)){
		x = bignum_mod(f_bignum(x),n);
		y = bignum_mod(f_bignum(f_bignum(y)),n);
		d = bignum_gcd(bignum_sub(x,y),n);
	}
	bignum_destroy(x);
	bignum_destroy(y);
	return d;
}



int main() {
  // En utilisant l'algorithme rho de Pollard, factorisez les entiers suivants

  unsigned long int n1 = 17 * 113;
  unsigned long int n2 = 239 * 431;
  unsigned long int n3 = 3469 * 4363;
  unsigned long int n4 = 15241 * 18119;
  unsigned long int n5 = 366127l * 416797l;
  unsigned long int n6 = 15651941l * 15485863l;

  bignum_t n7, n8;
 
  n7 = bignum_mul(bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(127)),
                             bignum_from_int(1)),
                  bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(61)),
                             bignum_from_int(1)));

  n8 = bignum_mul(bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(607)),
                             bignum_from_int(1)),
                  bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(2203)),
                             bignum_from_int(1)));


  
  printf("PGCD(42,24) = %lu\n", gcd(42,24));
  printf("PGCD(42,24) = %s\n", bignum_to_str(bignum_gcd(bignum_from_int(42),bignum_from_int(24))));
  printf("%s\n", bignum_to_str(floyd_bignum(n7)));
  
  return 0;
}
